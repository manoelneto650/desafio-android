package com.desafio.dribbble.desafio_android.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

/**
 * Created by Felipe on 15/08/2015.
 */
public class DownloadPictureShotTask extends AsyncTask<String, Integer, Bitmap> {

    private final WeakReference<ImageView> imageView;
    private final WeakReference<Context> contexto;


    public DownloadPictureShotTask(Context context, ImageView imageView){
        contexto =  new WeakReference<Context>(context);
        this.imageView = new WeakReference<ImageView>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String url = params[0];
        try {
            return BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        imageView.get().setImageBitmap(bitmap);
    }

}
