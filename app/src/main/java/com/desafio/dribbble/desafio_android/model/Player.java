package com.desafio.dribbble.desafio_android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ManoelNeto on 08/09/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Player implements Serializable {

    private Long id;
    private String name;
//    private String location;
//    private int likes_count;
//    private int likes_received_count;
//    private String url;
    private String avatar_url;

    public Player() {
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
//
//    @JsonProperty("location")
//    public String getLocation() {
//        return location;
//    }
//
//    public void setLocation(String location) {
//        this.location = location;
//    }
//
//    @JsonProperty("likes_count")
//    public int getLikes_count() {
//        return likes_count;
//    }
//
//    public void setLikes_count(int likes_count) {
//        this.likes_count = likes_count;
//    }
//
//    @JsonProperty("likes_received_count")
//    public int getLikes_received_count() {
//        return likes_received_count;
//    }
//
//    public void setLikes_received_count(int likes_received_count) {
//        this.likes_received_count = likes_received_count;
//    }
//
//    @JsonProperty("url")
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
    @JsonProperty("avatar_url")
    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }


    @Override
    public String toString(){
        return "player: { " +
                    "id:" + getId()     +
//                     ", name:'" + getName() + '\'' +
//                     ", location:'" + getLocation() + '\'' +
//                    ",likes_count: "+ getLikes_count() +
//                    ",likes_received_count:" + getLikes_received_count() +
//                    ", url:'" + getUrl() + '\'' +
//                    ", avatar_url:'" + getAvatar_url() + '\'' +
                '}';
    }

    public static Player parse(JSONObject json) throws  Exception{
        Player retorno = new Player();

        retorno.setId((!"null".equals(json.getString("id"))) ? Long.parseLong(json.getString("id")) : null);
//        retorno.setName(json.getString("name"));
//        retorno.setLocation(json.getString("location"));
//        retorno.setLikes_count(json.getInt("likes_count"));
//        retorno.setLikes_received_count(json.getInt("likes_received_count"));
//        retorno.setUrl(json.getString("url"));
//        retorno.setAvatar_url(json.getString("avatar_url"));


        return retorno;
    }
}
