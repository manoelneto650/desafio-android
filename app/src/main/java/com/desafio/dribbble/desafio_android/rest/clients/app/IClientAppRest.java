package com.desafio.dribbble.desafio_android.rest.clients.app;

import com.desafio.dribbble.desafio_android.Utils.Utils;
import com.desafio.dribbble.desafio_android.model.Popular;
import com.desafio.dribbble.desafio_android.model.Shots;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * Created by Manoel Neto on 08/09/2015.
 */


@Rest(rootUrl = "http://api.dribbble.com/shots", converters = {MappingJackson2HttpMessageConverter.class})
public interface IClientAppRest {

    @Get("/popular?page=1")
    Popular shotsPopulares();

    @Get("/{id}")
    Shots shotPorId(Long id);
}
