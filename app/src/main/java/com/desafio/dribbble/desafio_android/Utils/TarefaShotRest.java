package com.desafio.dribbble.desafio_android.Utils;

import android.os.AsyncTask;

import com.desafio.dribbble.desafio_android.model.Popular;
import com.desafio.dribbble.desafio_android.rest.clients.app.IClientAppRest;

/**
 * Created by Manoel Neto on 11/09/15.
 */
public class TarefaShotRest extends AsyncTask<String, Void, Object > {

    private IClientAppRest restClient;


    public TarefaShotRest(IClientAppRest restClient){
        this.restClient = restClient;
    }

    @Override
    protected Object doInBackground(String... params) {
        Popular popular = restClient.shotsPopulares();
        return popular;
    }

    @Override
    protected void onPostExecute(Object object) {
        super.onPostExecute(object);
    }

    public IClientAppRest getRestClient() {
        return restClient;
    }

    public void setRestClient(IClientAppRest restClient) {
        this.restClient = restClient;
    }

}
