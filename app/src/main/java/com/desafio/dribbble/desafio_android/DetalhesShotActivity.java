package com.desafio.dribbble.desafio_android;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.desafio.dribbble.desafio_android.Utils.DownloadPictureShotTask;
import com.desafio.dribbble.desafio_android.Utils.TarefaShotRest;
import com.desafio.dribbble.desafio_android.adapters.ShotsAdapter;
import com.desafio.dribbble.desafio_android.model.Popular;
import com.desafio.dribbble.desafio_android.model.Shots;
import com.desafio.dribbble.desafio_android.rest.clients.app.IClientAppRest;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.concurrent.ExecutionException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ManoelNeto on 10/09/15.
 */

@EActivity(R.layout.activity_detalhes_shot)
public class DetalhesShotActivity extends AppCompatActivity {

    @ViewById(R.id.profile_image)
    protected CircleImageView profile_image;

    @ViewById(R.id.nome_player)
    protected TextView nome_player;

    @ViewById(R.id.title_shot)
    protected TextView title_shot;

    @ViewById(R.id.image_shot_detalhes)
    protected ImageView imageShotDetalhes;

    @Extra("parceiroSelecionado")
    Shots shotSelecionado;

    @RestService
    protected IClientAppRest serviceRest;

    @AfterViews
    void afterViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        try {
            getShots();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setaTextos(final Shots shotSelect){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nome_player.setText(shotSelect.getPlayer().getName());
                title_shot.setText(shotSelect.getTitle());
                //chama a tarefa para baixar a foto do player
                DownloadPictureShotTask task = new DownloadPictureShotTask(getApplicationContext(), profile_image);
                task.execute(shotSelect.getPlayer().getAvatar_url());

                DownloadPictureShotTask task1 = new DownloadPictureShotTask(getApplicationContext(), imageShotDetalhes);
                task1.execute(shotSelect.getImage_url());
            }
        });
    }

    private void getShots() throws ExecutionException, InterruptedException {
        new TarefaShotRest(serviceRest){

            @Override
            protected Object doInBackground(String... params) {
                return getRestClient().shotPorId(shotSelecionado.getId());
            }

            @Override
            protected void onPostExecute(Object object) {
                super.onPostExecute(object);
                setaTextos((Shots) object);
            }
        }.execute();
    }




}
