package com.desafio.dribbble.desafio_android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ManoelNeto on 08/09/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Popular implements Serializable {

    private static final long serialVersionUID = 1L;

    private int page;
    private int per_page;
    private int pages;
    private int total;
    private Shots[] shots;

    public Popular(){

    }

    @JsonProperty("page")
    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @JsonProperty("per_page")
    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    @JsonProperty("pages")
    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @JsonProperty("total")
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @JsonProperty("shots")
    public Shots[] getShots() {
        return shots;
    }

    public void setShots(Shots[] shots) {
        this.shots = shots;
    }

    @Override
    public String toString(){
        return "{ " +
                "page:"     + getPage()     +
                "per_page:" + getPer_page() +
                "pages:"    + getPages()    +
                "total:"    + getTotal()    +
                '}';
    }

    public static Popular parse(JSONObject json) throws  Exception{
        Popular retorno = new Popular();

        retorno.setPage(json.getInt("page"));
        retorno.setPer_page(json.getInt("per_page"));
        retorno.setPages(json.getInt("pages"));
        retorno.setTotal(json.getInt("total"));


        return retorno;
    }


}
