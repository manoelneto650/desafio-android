package com.desafio.dribbble.desafio_android;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.desafio.dribbble.desafio_android.Utils.TarefaShotRest;
import com.desafio.dribbble.desafio_android.adapters.ShotsAdapter;
import com.desafio.dribbble.desafio_android.model.Popular;
import com.desafio.dribbble.desafio_android.rest.clients.app.IClientAppRest;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.concurrent.ExecutionException;

/**
 * Created by ManoelNeto on 11/09/15.
 */

@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment {

    @ViewById(R.id.rv_list)
    protected RecyclerView mRecyclerView;

    @RestService
    protected IClientAppRest serviceRest;



    @AfterViews
    void afterView(){

        GridLayoutManager llm = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setHasFixedSize(true); //dizendo que o tamanho do RecyclerView nao vai mudar, otimizando mais ainda.
        mRecyclerView.setLayoutManager(llm);

        try {
            getShotsPopulares();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    private void getShotsPopulares() throws ExecutionException, InterruptedException {
        new TarefaShotRest(serviceRest){
            @Override
            protected void onPostExecute(final Object object) {
                super.onPostExecute(object);

                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        ShotsAdapter adapter = new ShotsAdapter(getActivity(), ((Popular) object).getShots());
                        mRecyclerView.setAdapter(adapter);
                        ((MainActivity) getActivity()).desabilitaProgress();
                    }
                });
            }
        }.execute();

    }
}
