package com.desafio.dribbble.desafio_android;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.desafio.dribbble.desafio_android.adapters.ShotsAdapter;
import com.desafio.dribbble.desafio_android.model.Popular;
import com.desafio.dribbble.desafio_android.model.Shots;
import com.desafio.dribbble.desafio_android.rest.clients.app.IClientAppRest;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import java.util.concurrent.ExecutionException;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.corpoDialogLogin)
    protected LinearLayout overlayProgress;

    @ViewById(R.id.progressBar)
    protected ProgressWheel progressBar;

    private boolean primeiraVez = true;

    @AfterViews
    void afterViews(){
        habilitaProgress();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("primeiravez", primeiraVez);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        primeiraVez = outState.getBoolean("primeiravez");
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (primeiraVez) {
            Fragment fragment = new MainFragment_();
            // update the main content by replacing fragments
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.commit();
            primeiraVez = false;
        }

    }

    public void habilitaProgress(){
        progressBar.setVisibility(View.VISIBLE);
        overlayProgress.setVisibility(View.VISIBLE);
        overlayProgress.setBackgroundColor(Color.TRANSPARENT);
        overlayProgress.bringToFront();
    }

    public void desabilitaProgress(){
        progressBar.setVisibility(View.INVISIBLE);
        overlayProgress.setVisibility(View.INVISIBLE);
    }

}


















