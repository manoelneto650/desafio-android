package com.desafio.dribbble.desafio_android.interfaces;

import android.view.View;

/**
 * Created by Manoel Neto on 09/09/2015.
 */
public interface RecyclerViewOnClickListenerHack {
    public void onClickListener(View view, int position);
    public void onLongPressClickListener(View view, int position);
}
