package com.desafio.dribbble.desafio_android.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by ManoelNeto on 08/09/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Shots implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    //private String description;
//    private int height;
//    private int width;
    private int likes_count;
    private String url;
//    private String short_url;
    private int views_count;
    private String image_url;
//    private String image_teaser_url;
    private Player player;

    public Shots() {
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    @JsonProperty("title")
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    @JsonProperty("title")
//    public int getHeight() {
//        return height;
//    }
//
//    public void setHeight(int height) {
//        this.height = height;
//    }
//
//    @JsonProperty("title")
//    public int getWidth() {
//        return width;
//    }
//
//    public void setWidth(int width) {
//        this.width = width;
//    }
//
    @JsonProperty("likes_count")
    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

//    @JsonProperty("short_url")
//    public String getShort_url() {
//        return short_url;
//    }
//
//    public void setShort_url(String short_url) {
//        this.short_url = short_url;
//    }
//
    @JsonProperty("views_count")
    public int getViews_count() {
        return views_count;
    }

    public void setViews_count(int views_count) {
        this.views_count = views_count;
    }

    @JsonProperty("image_url")
    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

//    @JsonProperty("image_teaser_url")
//    public String getImage_teaser_url() {
//        return image_teaser_url;
//    }
//
//    public void setImage_teaser_url(String image_teaser_url) {
//        this.image_teaser_url = image_teaser_url;
//    }

    @JsonProperty("player")
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString(){
        return "player: { " +



                '}';
    }

    public static Shots parse(JSONObject json) throws  Exception{
        Shots retorno = new Shots();

        retorno.setId((!"null".equals(json.getString("id"))) ? Long.parseLong(json.getString("id")) : null);
//        retorno.setName(json.getString("name"));
//        retorno.setLocation(json.getString("location"));
//        retorno.setLikes_count(json.getInt("likes_count"));
//        retorno.setLikes_received_count(json.getInt("likes_received_count"));
//        retorno.setUrl(json.getString("url"));
//        retorno.setAvatar_url(json.getString("avatar_url"));

        return retorno;
    }


}
