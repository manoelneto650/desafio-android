package com.desafio.dribbble.desafio_android.Utils;

/**
 * Created by Manoel Neto on 08/09/2015.
 */
public class Utils {

    public static final String APP_SERVER = "http://192.168.0.16:8080/BuscaServiceUp/";
    public static final String APP_AUTH_URL = "j_spring_security_check";

    public static String ShotsPopular = "{\n" +
            "    \"page\": \"1\",\n" +
            "    \"per_page\": 15,\n" +
            "    \"pages\": 50,\n" +
            "    \"total\": 750,\n" +
            "    \"shots\": [\n" +
            "        {\n" +
            "            \"id\": 2234290,\n" +
            "            \"title\": \"Tuff Shed · Couples Retreat\",\n" +
            "            \"description\": \"<p>New outdoor illustration work for Colorado based Tuff Shed! Check out the creation of this series here: http://orlincultureshop.com/blog/2015/9/7/tuff-shed</p>\\n\\n<p>This particular piece was one of my favorites to work on. Hope your projects are going well!</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 326,\n" +
            "            \"comments_count\": 11,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234290-Tuff-Shed-Couples-Retreat\",\n" +
            "            \"short_url\": \"http://drbl.in/pUpg\",\n" +
            "            \"views_count\": 1516,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234290/bemocs_tuff_shed_couples_retreat_800x600.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234290/bemocs_tuff_shed_couples_retreat_800x600_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234290/bemocs_tuff_shed_couples_retreat_800x600_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 329207,\n" +
            "                \"name\": \"Brian Edward Miller\",\n" +
            "                \"location\": \"Erie, CO\",\n" +
            "                \"followers_count\": 4499,\n" +
            "                \"draftees_count\": 0,\n" +
            "                \"likes_count\": 25,\n" +
            "                \"likes_received_count\": 14008,\n" +
            "                \"comments_count\": 96,\n" +
            "                \"comments_received_count\": 813,\n" +
            "                \"rebounds_count\": 0,\n" +
            "                \"rebounds_received_count\": 1,\n" +
            "                \"url\": \"http://dribbble.com/bemocs\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/avatars/normal/ocs_logo_2014.png?1398296680\",\n" +
            "                \"username\": \"bemocs\",\n" +
            "                \"twitter_screen_name\": \"bemocs\",\n" +
            "                \"website_url\": \"http://www.orlincultureshop.com\",\n" +
            "                \"drafted_by_player_id\": 212283,\n" +
            "                \"shots_count\": 61,\n" +
            "                \"following_count\": 17,\n" +
            "                \"created_at\": \"2013/05/06 23:19:54 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:37:48 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234274,\n" +
            "            \"title\": \"Touchdown!\",\n" +
            "            \"description\": \"<p>We recently did some cool animation work for an American Television brand as part of an NFL campaign.\\n<br />This is just a small snippet - full project soon to come.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 285,\n" +
            "            \"comments_count\": 20,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234274-Touchdown\",\n" +
            "            \"short_url\": \"http://drbl.in/pUoQ\",\n" +
            "            \"views_count\": 1635,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/99875/screenshots/2234274/spike-64-2.gif\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/99875/screenshots/2234274/spike-64-2_teaser.gif\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/99875/screenshots/2234274/spike-64-2_1x.gif\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 99875,\n" +
            "                \"name\": \"R A D I O\",\n" +
            "                \"location\": \"Cape Town \",\n" +
            "                \"followers_count\": 12143,\n" +
            "                \"draftees_count\": 4,\n" +
            "                \"likes_count\": 614,\n" +
            "                \"likes_received_count\": 48155,\n" +
            "                \"comments_count\": 129,\n" +
            "                \"comments_received_count\": 2206,\n" +
            "                \"rebounds_count\": 6,\n" +
            "                \"rebounds_received_count\": 10,\n" +
            "                \"url\": \"http://dribbble.com/madebyradio\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/99875/avatars/normal/b67a43042c708579043891bfac086046.jpg?1437410213\",\n" +
            "                \"username\": \"madebyradio\",\n" +
            "                \"twitter_screen_name\": \"madebyradio\",\n" +
            "                \"website_url\": \"http://www.madebyradio.com\",\n" +
            "                \"drafted_by_player_id\": 196,\n" +
            "                \"shots_count\": 156,\n" +
            "                \"following_count\": 740,\n" +
            "                \"created_at\": \"2012/02/07 01:34:28 -0500\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:31:42 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234227,\n" +
            "            \"title\": \"Wizard Beats\",\n" +
            "            \"description\": \"<p>Just a wizard rockin' out to some trippy beats</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 276,\n" +
            "            \"comments_count\": 12,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234227-Wizard-Beats\",\n" +
            "            \"short_url\": \"http://drbl.in/pUnV\",\n" +
            "            \"views_count\": 1548,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/59947/screenshots/2234227/wizard-dribbble.gif\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/59947/screenshots/2234227/wizard-dribbble_teaser.gif\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/59947/screenshots/2234227/wizard-dribbble_1x.gif\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 59947,\n" +
            "                \"name\": \"MUTI\",\n" +
            "                \"location\": \"Cape Town, South Africa\",\n" +
            "                \"followers_count\": 11830,\n" +
            "                \"draftees_count\": 2,\n" +
            "                \"likes_count\": 771,\n" +
            "                \"likes_received_count\": 95616,\n" +
            "                \"comments_count\": 264,\n" +
            "                \"comments_received_count\": 3915,\n" +
            "                \"rebounds_count\": 169,\n" +
            "                \"rebounds_received_count\": 179,\n" +
            "                \"url\": \"http://dribbble.com/studioMUTI\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/59947/avatars/normal/MUTI-01.jpg?1390990460\",\n" +
            "                \"username\": \"studioMUTI\",\n" +
            "                \"twitter_screen_name\": \"StudioMuti\",\n" +
            "                \"website_url\": \"http://www.studiomuti.co.za\",\n" +
            "                \"drafted_by_player_id\": 6133,\n" +
            "                \"shots_count\": 593,\n" +
            "                \"following_count\": 209,\n" +
            "                \"created_at\": \"2011/09/06 12:01:24 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:08:44 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234287,\n" +
            "            \"title\": \"Tuff Shed · Family Vacation\",\n" +
            "            \"description\": \"<p>New outdoor illustration work for Colorado based Tuff Shed! Check out the creation of this series here: http://orlincultureshop.com/blog/2015/9/7/tuff-shed</p>\\n\\n<p>Hope your projects are going well!</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 267,\n" +
            "            \"comments_count\": 11,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234287-Tuff-Shed-Family-Vacation\",\n" +
            "            \"short_url\": \"http://drbl.in/pUpd\",\n" +
            "            \"views_count\": 1347,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234287/bemocs_tuff_shed_family_vacation_800x600.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234287/bemocs_tuff_shed_family_vacation_800x600_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/screenshots/2234287/bemocs_tuff_shed_family_vacation_800x600_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 329207,\n" +
            "                \"name\": \"Brian Edward Miller\",\n" +
            "                \"location\": \"Erie, CO\",\n" +
            "                \"followers_count\": 4499,\n" +
            "                \"draftees_count\": 0,\n" +
            "                \"likes_count\": 25,\n" +
            "                \"likes_received_count\": 14008,\n" +
            "                \"comments_count\": 96,\n" +
            "                \"comments_received_count\": 813,\n" +
            "                \"rebounds_count\": 0,\n" +
            "                \"rebounds_received_count\": 1,\n" +
            "                \"url\": \"http://dribbble.com/bemocs\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/329207/avatars/normal/ocs_logo_2014.png?1398296680\",\n" +
            "                \"username\": \"bemocs\",\n" +
            "                \"twitter_screen_name\": \"bemocs\",\n" +
            "                \"website_url\": \"http://www.orlincultureshop.com\",\n" +
            "                \"drafted_by_player_id\": 212283,\n" +
            "                \"shots_count\": 61,\n" +
            "                \"following_count\": 17,\n" +
            "                \"created_at\": \"2013/05/06 23:19:54 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:37:11 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234007,\n" +
            "            \"title\": \"Bird Tree Feeder\",\n" +
            "            \"description\": \"<p>My favourite clip from my current project. SQUARK!</p>\\n\\n<p>Project is nearly complete so I should be able to show it in a few weeks.</p>\\n\\n<p>P.S The gif quality is a bit rubbish</p>\",\n" +
            "            \"height\": 300,\n" +
            "            \"width\": 400,\n" +
            "            \"likes_count\": 211,\n" +
            "            \"comments_count\": 17,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234007-Bird-Tree-Feeder\",\n" +
            "            \"short_url\": \"http://drbl.in/pUjJ\",\n" +
            "            \"views_count\": 1393,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/6063/screenshots/2234007/bird_tree.gif\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/6063/screenshots/2234007/bird_tree_teaser.gif\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 6063,\n" +
            "                \"name\": \"Oliver Sin\",\n" +
            "                \"location\": \"Cheltenham, UK\",\n" +
            "                \"followers_count\": 2870,\n" +
            "                \"draftees_count\": 8,\n" +
            "                \"likes_count\": 1927,\n" +
            "                \"likes_received_count\": 12848,\n" +
            "                \"comments_count\": 807,\n" +
            "                \"comments_received_count\": 1220,\n" +
            "                \"rebounds_count\": 5,\n" +
            "                \"rebounds_received_count\": 4,\n" +
            "                \"url\": \"http://dribbble.com/oliversin\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/6063/avatars/normal/NEW_TWIT_AVAT.png?1366712872\",\n" +
            "                \"username\": \"oliversin\",\n" +
            "                \"twitter_screen_name\": \"oliversin\",\n" +
            "                \"website_url\": \"http://www.oliversin.com\",\n" +
            "                \"drafted_by_player_id\": 2161,\n" +
            "                \"shots_count\": 162,\n" +
            "                \"following_count\": 520,\n" +
            "                \"created_at\": \"2010/12/13 11:53:22 -0500\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 06:21:10 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234422,\n" +
            "            \"title\": \"Agent Pekka.\",\n" +
            "            \"description\": \"<p><a href=\\\"http://www.agentpekka.com\\\" rel=\\\"nofollow\\\">Agent Pekka</a> just launched their brand new website and I made these illustrations for the new <a href=\\\"http://agentpekka.com/about\\\" rel=\\\"nofollow\\\">About page</a>.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 184,\n" +
            "            \"comments_count\": 9,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234422-Agent-Pekka\",\n" +
            "            \"short_url\": \"http://drbl.in/pUrI\",\n" +
            "            \"views_count\": 993,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/2489/screenshots/2234422/agent-pekka-gif.gif\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/2489/screenshots/2234422/agent-pekka-gif_teaser.gif\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/2489/screenshots/2234422/agent-pekka-gif_1x.gif\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 2489,\n" +
            "                \"name\": \"Tim Boelaars\",\n" +
            "                \"location\": \"Amsterdam\",\n" +
            "                \"followers_count\": 21642,\n" +
            "                \"draftees_count\": 14,\n" +
            "                \"likes_count\": 5299,\n" +
            "                \"likes_received_count\": 54762,\n" +
            "                \"comments_count\": 1092,\n" +
            "                \"comments_received_count\": 3862,\n" +
            "                \"rebounds_count\": 51,\n" +
            "                \"rebounds_received_count\": 87,\n" +
            "                \"url\": \"http://dribbble.com/timboelaars\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/2489/avatars/normal/avatar.jpg?1391435914\",\n" +
            "                \"username\": \"timboelaars\",\n" +
            "                \"twitter_screen_name\": \"timboelaars\",\n" +
            "                \"website_url\": \"http://timboelaars.nl\",\n" +
            "                \"drafted_by_player_id\": 1985,\n" +
            "                \"shots_count\": 262,\n" +
            "                \"following_count\": 464,\n" +
            "                \"created_at\": \"2010/06/03 05:08:59 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 10:32:01 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234251,\n" +
            "            \"title\": \"Gimmecar\",\n" +
            "            \"description\": \"<p>A little side experiment.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 185,\n" +
            "            \"comments_count\": 11,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234251-Gimmecar\",\n" +
            "            \"short_url\": \"http://drbl.in/pUot\",\n" +
            "            \"views_count\": 1687,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/1061/screenshots/2234251/gimmecar.png\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/1061/screenshots/2234251/gimmecar_teaser.png\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/1061/screenshots/2234251/gimmecar_1x.png\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 1061,\n" +
            "                \"name\": \"Fabio Basile\",\n" +
            "                \"location\": \"London, UK\",\n" +
            "                \"followers_count\": 19368,\n" +
            "                \"draftees_count\": 24,\n" +
            "                \"likes_count\": 11680,\n" +
            "                \"likes_received_count\": 45607,\n" +
            "                \"comments_count\": 900,\n" +
            "                \"comments_received_count\": 2907,\n" +
            "                \"rebounds_count\": 10,\n" +
            "                \"rebounds_received_count\": 74,\n" +
            "                \"url\": \"http://dribbble.com/toxinide\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/1061/avatars/normal/681819e188a235663ac3e72820e0209e.png?1432752140\",\n" +
            "                \"username\": \"toxinide\",\n" +
            "                \"twitter_screen_name\": \"fffabs\",\n" +
            "                \"website_url\": \"http://fabio.cool\",\n" +
            "                \"drafted_by_player_id\": 94,\n" +
            "                \"shots_count\": 151,\n" +
            "                \"following_count\": 1796,\n" +
            "                \"created_at\": \"2010/03/18 09:59:16 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:18:29 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234658,\n" +
            "            \"title\": \"Fruit & Grain Clothing Label\",\n" +
            "            \"description\": \"<p>Part of some packaging/labels for F&amp;G. This is for some shirts they will be selling.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 174,\n" +
            "            \"comments_count\": 4,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234658-Fruit-Grain-Clothing-Label\",\n" +
            "            \"short_url\": \"http://drbl.in/pUwk\",\n" +
            "            \"views_count\": 803,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/52084/screenshots/2234658/g_g.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/52084/screenshots/2234658/g_g_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/52084/screenshots/2234658/g_g_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 52084,\n" +
            "                \"name\": \"Steve Wolf\",\n" +
            "                \"location\": \"Austin, Texas\",\n" +
            "                \"followers_count\": 8913,\n" +
            "                \"draftees_count\": 8,\n" +
            "                \"likes_count\": 771,\n" +
            "                \"likes_received_count\": 46842,\n" +
            "                \"comments_count\": 1261,\n" +
            "                \"comments_received_count\": 2538,\n" +
            "                \"rebounds_count\": 0,\n" +
            "                \"rebounds_received_count\": 20,\n" +
            "                \"url\": \"http://dribbble.com/WOLF_STEVE\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/52084/avatars/normal/ef07fd4b5e3d27774b8c077cc61d26d8.jpg?1411136929\",\n" +
            "                \"username\": \"WOLF_STEVE\",\n" +
            "                \"twitter_screen_name\": \"STEVEWOLFDESIGN\",\n" +
            "                \"website_url\": \"http://stevewolf.co/\",\n" +
            "                \"drafted_by_player_id\": 18850,\n" +
            "                \"shots_count\": 210,\n" +
            "                \"following_count\": 797,\n" +
            "                \"created_at\": \"2011/08/10 15:36:38 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 12:15:10 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234655,\n" +
            "            \"title\": \"Parrot\",\n" +
            "            \"description\": \"<p>A thing that got nixed. But the new thing for the same thing looks way better, so I ain't cross.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 155,\n" +
            "            \"comments_count\": 6,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234655-Parrot\",\n" +
            "            \"short_url\": \"http://drbl.in/pUwh\",\n" +
            "            \"views_count\": 555,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/52758/screenshots/2234655/parrot_j_fletcher_dribbble.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/52758/screenshots/2234655/parrot_j_fletcher_dribbble_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/52758/screenshots/2234655/parrot_j_fletcher_dribbble_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 52758,\n" +
            "                \"name\": \"Jay Fletcher\",\n" +
            "                \"location\": \"Charleston, SC\",\n" +
            "                \"followers_count\": 11811,\n" +
            "                \"draftees_count\": 9,\n" +
            "                \"likes_count\": 6184,\n" +
            "                \"likes_received_count\": 84173,\n" +
            "                \"comments_count\": 292,\n" +
            "                \"comments_received_count\": 4724,\n" +
            "                \"rebounds_count\": 189,\n" +
            "                \"rebounds_received_count\": 291,\n" +
            "                \"url\": \"http://dribbble.com/jfletcherdesign\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/52758/avatars/normal/J_FLETCHER_DESIGN_LOGO-01.jpg?1401983283\",\n" +
            "                \"username\": \"jfletcherdesign\",\n" +
            "                \"twitter_screen_name\": \"jfletcherdesign\",\n" +
            "                \"website_url\": \"http://www.jfletcherdesign.com\",\n" +
            "                \"drafted_by_player_id\": 4018,\n" +
            "                \"shots_count\": 422,\n" +
            "                \"following_count\": 546,\n" +
            "                \"created_at\": \"2011/08/13 19:30:38 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 12:13:44 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234291,\n" +
            "            \"title\": \"Skullz\",\n" +
            "            \"description\": null,\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 154,\n" +
            "            \"comments_count\": 5,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234291-Skullz\",\n" +
            "            \"short_url\": \"http://drbl.in/pUph\",\n" +
            "            \"views_count\": 745,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/74401/screenshots/2234291/skullz.png\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/74401/screenshots/2234291/skullz_teaser.png\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/74401/screenshots/2234291/skullz_1x.png\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 74401,\n" +
            "                \"name\": \"Stevan Rodic\",\n" +
            "                \"location\": \"Belgrade, Serbia\",\n" +
            "                \"followers_count\": 6356,\n" +
            "                \"draftees_count\": 8,\n" +
            "                \"likes_count\": 6887,\n" +
            "                \"likes_received_count\": 42673,\n" +
            "                \"comments_count\": 762,\n" +
            "                \"comments_received_count\": 3182,\n" +
            "                \"rebounds_count\": 41,\n" +
            "                \"rebounds_received_count\": 45,\n" +
            "                \"url\": \"http://dribbble.com/Stevan\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/74401/avatars/normal/STEVA_konstantan_200_X_200_PX.gif?1398280495\",\n" +
            "                \"username\": \"Stevan\",\n" +
            "                \"twitter_screen_name\": null,\n" +
            "                \"website_url\": \"http://www.stvdesign.com \",\n" +
            "                \"drafted_by_player_id\": 47731,\n" +
            "                \"shots_count\": 332,\n" +
            "                \"following_count\": 943,\n" +
            "                \"created_at\": \"2011/11/09 09:25:10 -0500\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 09:38:11 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2233991,\n" +
            "            \"title\": \"Explore\",\n" +
            "            \"description\": \"<p>This is the landing page for a site detailing the history of discovery. </p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 160,\n" +
            "            \"comments_count\": 6,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2233991-Explore\",\n" +
            "            \"short_url\": \"http://drbl.in/pUjt\",\n" +
            "            \"views_count\": 1442,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/230268/screenshots/2233991/exlore.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/230268/screenshots/2233991/exlore_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/230268/screenshots/2233991/exlore_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 230268,\n" +
            "                \"name\": \"Samson Vowles\",\n" +
            "                \"location\": \"London\",\n" +
            "                \"followers_count\": 979,\n" +
            "                \"draftees_count\": 0,\n" +
            "                \"likes_count\": 3976,\n" +
            "                \"likes_received_count\": 1298,\n" +
            "                \"comments_count\": 224,\n" +
            "                \"comments_received_count\": 87,\n" +
            "                \"rebounds_count\": 0,\n" +
            "                \"rebounds_received_count\": 0,\n" +
            "                \"url\": \"http://dribbble.com/vowlesyy\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/230268/avatars/normal/55b3931dfb563c18e1780c1afa631f2e.png?1416175699\",\n" +
            "                \"username\": \"vowlesyy\",\n" +
            "                \"twitter_screen_name\": \"vowlesyy\",\n" +
            "                \"website_url\": null,\n" +
            "                \"drafted_by_player_id\": 323382,\n" +
            "                \"shots_count\": 25,\n" +
            "                \"following_count\": 1712,\n" +
            "                \"created_at\": \"2012/10/31 13:09:30 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 06:12:59 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234198,\n" +
            "            \"title\": \"Type Growler Series\",\n" +
            "            \"description\": \"<p>My final beer growler series triptych! Happy to see it all come together.</p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 130,\n" +
            "            \"comments_count\": 3,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234198-Type-Growler-Series\",\n" +
            "            \"short_url\": \"http://drbl.in/pUns\",\n" +
            "            \"views_count\": 672,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/77241/screenshots/2234198/type-growler-series-dribbb.jpg\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/77241/screenshots/2234198/type-growler-series-dribbb_teaser.jpg\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/77241/screenshots/2234198/type-growler-series-dribbb_1x.jpg\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 77241,\n" +
            "                \"name\": \"Wells\",\n" +
            "                \"location\": \"Atlanta, GA\",\n" +
            "                \"followers_count\": 2200,\n" +
            "                \"draftees_count\": 2,\n" +
            "                \"likes_count\": 7273,\n" +
            "                \"likes_received_count\": 11841,\n" +
            "                \"comments_count\": 1008,\n" +
            "                \"comments_received_count\": 934,\n" +
            "                \"rebounds_count\": 10,\n" +
            "                \"rebounds_received_count\": 10,\n" +
            "                \"url\": \"http://dribbble.com/wellscollins\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/77241/avatars/normal/bio-pic-3.png?1390258003\",\n" +
            "                \"username\": \"wellscollins\",\n" +
            "                \"twitter_screen_name\": \"wellscollins\",\n" +
            "                \"website_url\": \"http://wellscollins.com\",\n" +
            "                \"drafted_by_player_id\": 61834,\n" +
            "                \"shots_count\": 173,\n" +
            "                \"following_count\": 674,\n" +
            "                \"created_at\": \"2011/11/21 14:33:37 -0500\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 08:55:46 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234695,\n" +
            "            \"title\": \"Label Illustrations\",\n" +
            "            \"description\": \"<p>Finally I made a composition with all the illustration for Kaffe1668 juice bottles. <a href=\\\"https://dribbble.com/shots/2234695-Dr/attachments/416202\\\" rel=\\\"nofollow\\\">Big version here.</a></p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 114,\n" +
            "            \"comments_count\": 4,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234695-Label-Illustrations\",\n" +
            "            \"short_url\": \"http://drbl.in/pUwV\",\n" +
            "            \"views_count\": 506,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/73492/screenshots/2234695/dr.png\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/73492/screenshots/2234695/dr_teaser.png\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/73492/screenshots/2234695/dr_1x.png\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 73492,\n" +
            "                \"name\": \"Martin Azambuja\",\n" +
            "                \"location\": \"Montevideo, Uruguay\",\n" +
            "                \"followers_count\": 6001,\n" +
            "                \"draftees_count\": 8,\n" +
            "                \"likes_count\": 4480,\n" +
            "                \"likes_received_count\": 30296,\n" +
            "                \"comments_count\": 560,\n" +
            "                \"comments_received_count\": 1697,\n" +
            "                \"rebounds_count\": 26,\n" +
            "                \"rebounds_received_count\": 28,\n" +
            "                \"url\": \"http://dribbble.com/maz_martin\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/73492/avatars/normal/3965a71abc6b8c45f1159c2bfa75e6bb.jpg?1406784962\",\n" +
            "                \"username\": \"maz_martin\",\n" +
            "                \"twitter_screen_name\": \"maz_martin\",\n" +
            "                \"website_url\": \"http://www.martinazambuja.com\",\n" +
            "                \"drafted_by_player_id\": 39,\n" +
            "                \"shots_count\": 202,\n" +
            "                \"following_count\": 492,\n" +
            "                \"created_at\": \"2011/11/05 15:52:40 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 12:38:42 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234721,\n" +
            "            \"title\": \"Chalk Ink\",\n" +
            "            \"description\": \"<p>3-piece mural project for the Chalk Ink people in Austin.</p>\\n\\n<p>See the full project <a href=\\\"http://www.simonwalkertype.com/#/chalk-ink/\\\" rel=\\\"nofollow\\\">here.</a></p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 113,\n" +
            "            \"comments_count\": 8,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234721-Chalk-Ink\",\n" +
            "            \"short_url\": \"http://drbl.in/pUxv\",\n" +
            "            \"views_count\": 657,\n" +
            "            \"rebound_source_id\": null,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/1571/screenshots/2234721/screen_shot_2015-09-08_at_11.53.31_am.png\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/1571/screenshots/2234721/screen_shot_2015-09-08_at_11.53.31_am_teaser.png\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/1571/screenshots/2234721/screen_shot_2015-09-08_at_11.53.31_am_1x.png\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 1571,\n" +
            "                \"name\": \"Simon Walker\",\n" +
            "                \"location\": \"Austin, Texas\",\n" +
            "                \"followers_count\": 15918,\n" +
            "                \"draftees_count\": 11,\n" +
            "                \"likes_count\": 2686,\n" +
            "                \"likes_received_count\": 39217,\n" +
            "                \"comments_count\": 725,\n" +
            "                \"comments_received_count\": 3425,\n" +
            "                \"rebounds_count\": 0,\n" +
            "                \"rebounds_received_count\": 9,\n" +
            "                \"url\": \"http://dribbble.com/super_furry\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/1571/avatars/normal/5c8d083707a534956b81b1a80cdff4cc.jpg?1421772201\",\n" +
            "                \"username\": \"super_furry\",\n" +
            "                \"twitter_screen_name\": \"super_furry\",\n" +
            "                \"website_url\": \"http://simonwalkertype.com\",\n" +
            "                \"drafted_by_player_id\": 92,\n" +
            "                \"shots_count\": 154,\n" +
            "                \"following_count\": 1641,\n" +
            "                \"created_at\": \"2010/03/22 12:00:53 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 12:54:03 -0400\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 2234921,\n" +
            "            \"title\": \"Computer\",\n" +
            "            \"description\": \"<p>Some more fun stuff I made for @[718670:Identity Visuals] and @[55063:Zac Dixon]! Seriously, it was such a honor to work with them on this project. Check out their work. </p>\",\n" +
            "            \"height\": 600,\n" +
            "            \"width\": 800,\n" +
            "            \"likes_count\": 116,\n" +
            "            \"comments_count\": 6,\n" +
            "            \"rebounds_count\": 0,\n" +
            "            \"url\": \"http://dribbble.com/shots/2234921-Computer\",\n" +
            "            \"short_url\": \"http://drbl.in/pUBn\",\n" +
            "            \"views_count\": 606,\n" +
            "            \"rebound_source_id\": 2205163,\n" +
            "            \"image_url\": \"https://d13yacurqjgara.cloudfront.net/users/31752/screenshots/2234921/computer.png\",\n" +
            "            \"image_teaser_url\": \"https://d13yacurqjgara.cloudfront.net/users/31752/screenshots/2234921/computer_teaser.png\",\n" +
            "            \"image_400_url\": \"https://d13yacurqjgara.cloudfront.net/users/31752/screenshots/2234921/computer_1x.png\",\n" +
            "            \"player\": {\n" +
            "                \"id\": 31752,\n" +
            "                \"name\": \"Nick Slater\",\n" +
            "                \"location\": \"Palo Alto, Ca\",\n" +
            "                \"followers_count\": 35214,\n" +
            "                \"draftees_count\": 14,\n" +
            "                \"likes_count\": 46658,\n" +
            "                \"likes_received_count\": 219098,\n" +
            "                \"comments_count\": 7773,\n" +
            "                \"comments_received_count\": 14249,\n" +
            "                \"rebounds_count\": 263,\n" +
            "                \"rebounds_received_count\": 404,\n" +
            "                \"url\": \"http://dribbble.com/slaterdesign\",\n" +
            "                \"avatar_url\": \"https://d13yacurqjgara.cloudfront.net/users/31752/avatars/normal/312157471aa9ba31c98655135988e440.png?1441221178\",\n" +
            "                \"username\": \"slaterdesign\",\n" +
            "                \"twitter_screen_name\": \"slaterdesign\",\n" +
            "                \"website_url\": null,\n" +
            "                \"drafted_by_player_id\": 2045,\n" +
            "                \"shots_count\": 620,\n" +
            "                \"following_count\": 1681,\n" +
            "                \"created_at\": \"2011/04/30 19:12:27 -0400\"\n" +
            "            },\n" +
            "            \"created_at\": \"2015/09/08 14:51:08 -0400\"\n" +
            "        }\n" +
            "    ]\n" +
            "}";
}
