package com.desafio.dribbble.desafio_android.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.desafio.dribbble.desafio_android.DetalhesShotActivity_;
import com.desafio.dribbble.desafio_android.R;
import com.desafio.dribbble.desafio_android.Utils.DownloadPictureShotTask;
import com.desafio.dribbble.desafio_android.interfaces.RecyclerViewOnClickListenerHack;
import com.desafio.dribbble.desafio_android.model.Shots;


/**
 * Created by Manoel Neto on 09/09/2015.
 */
public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.MyViewHolder>{
    private Context context;
    private Shots[] mList;
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private float scale;
    private int width;
    private int height;

    public ShotsAdapter(Context c, Shots[] l){
        context = c;
        mList = l;
        mLayoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        scale = context.getResources().getDisplayMetrics().density;
        width = context.getResources().getDisplayMetrics().widthPixels - (int) (14 * scale + 0.5f);
        height = (width / 16) * 9;
    }

    //Quando a nescessidade de criar uma nova view
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_shot, viewGroup, false);
        return new MyViewHolder(v);
    }

    //Vincula os dados a View
    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int position) {
        myViewHolder.views_count.setText(String.valueOf(mList[position].getViews_count()));
        myViewHolder.likes_count.setText(String.valueOf(mList[position].getLikes_count()));

        //chama a tarefa para baixar a foto do shot
        DownloadPictureShotTask task = new DownloadPictureShotTask(context, myViewHolder.imageShot);
        task.execute(mList[position].getImage_url());
    }

    @Override
    public int getItemCount() {
        return mList.length;
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(Shots a, int position){
        mList[position] = a;
        notifyItemInserted(position);
    }

    public void removeListemItem(int position){
        mList[position] = null ;
        notifyItemRemoved(position);
    }

    //Vai trabalhar a Cash, guardar a View para ser reutilizada
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageShot;
        public TextView views_count;
        public TextView likes_count;
        public CardView card;

        public MyViewHolder(View itemView) {
            super(itemView);
            imageShot = (ImageView) itemView.findViewById(R.id.image_shot);
            views_count = (TextView) itemView.findViewById(R.id.views_count);
            likes_count = (TextView) itemView.findViewById(R.id.likes_count);

            itemView.setOnClickListener(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DetalhesShotActivity_.intent(context)
                            .flags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .shotSelecionado(mList[getPosition()])
                            .start();
                }
            });


        }

        @Override
        public void onClick(View v) {
            if (mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getPosition());
            }
        }
    }

}
